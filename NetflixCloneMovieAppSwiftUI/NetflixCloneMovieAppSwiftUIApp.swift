//
//  NetflixCloneMovieAppSwiftUIApp.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

@main
struct NetflixCloneMovieAppSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()                
            }
        }
    }
}
