//
//  ProfileView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct ProfileView: View {
    @State var userEmail : String = ""
    @State var userName : String = ""
    @State var selectedItem: Int = 0
    @State var selectedPaymentMethod: Int = 1
    
    @State var dataArray: [String] = ["₺ Turkish Lira", "$ US Dollar", "€ Euro"]
    
    @State var paymentMethodArray: [String] = ["Paypal", "Credit/Debit Card", "Bitcoin"]
    
    func getUser() {
        userName = "Onur K."
        userEmail = "ios.devops.pro@gmail.com"
    }
    
    var body: some View {
        VStack {
            HStack{Spacer()}
            NavHeaderSetView(btnImage: "arrow.left", headerLogo: "N", isBackButtonVisible: false)
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack (spacing: 20){
                    UserIconView()
                    VStack {
                        Text("\(userName)")
                            .foregroundColor(.white)
                            .font(.system(size: 35, weight: .bold, design: .rounded))
                        Text("\(userEmail)")
                            .foregroundColor(.white)
                            .font(.system(size: 25, weight: .bold, design: .rounded))
                    }
                    .padding()
                    .background(.ultraThinMaterial)
                    .cornerRadius(7)
                    Form {
                        Section(header: Text("User Settings")) {
                            SettingsView(sectionName: "Currency", selected: $selectedItem, dataArray: $dataArray)
                            SettingsView(sectionName: "Currency", selected: $selectedPaymentMethod, dataArray: $paymentMethodArray)
                        }
                    }
                    .background(.ultraThinMaterial)
                    .frame(height: 200)
                }
                
                
                Button {

                } label: {
                    CustomLoginButtonView(buttonText: "Log out", buttonTextColor: .white, buttonWidth: UIScreen.main.bounds.width - 50, buttonRadius: 10, buttonBg: .red)
                }
                
            }
        }
        .background(.black)
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .onAppear {
            getUser()
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}

struct Blur: UIViewRepresentable {
    var style: UIBlurEffect.Style = .systemMaterial

    func makeUIView(context: Context) -> UIVisualEffectView {
        return UIVisualEffectView(effect: UIBlurEffect(style: style))
    }

    func updateUIView(_ uiView: UIVisualEffectView, context: Context) {
        uiView.effect = UIBlurEffect(style: style)
    }
}
