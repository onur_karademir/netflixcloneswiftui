//
//  InfoView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct InfoView: View {
    @State var isLine : Bool = false
    var body: some View {
        VStack {
            NavHeaderSetView(btnImage: "arrow.left", headerLogo: "N", isBackButtonVisible: true)
                .padding(.top)
            ScrollView(.vertical, showsIndicators: false) {
                VStack (spacing : 0) {
                    NetflixTitleView(fontSize: 140, netflixN: "N")
                    VStack (alignment: .center, spacing: 20) {
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width : UIScreen.main.bounds.width - 350,  height: 10)
                            .background(.red)
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width - 100 : UIScreen.main.bounds.width - 350, height: 10)
                            .background(.red)
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width - 150 : UIScreen.main.bounds.width - 350, height: 10)
                            .background(.red)
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width - 200 : UIScreen.main.bounds.width - 350, height: 10)
                            .background(.red)
                    }
                    VStack {
                        InfoDetailView(
                            image: "person.fill",
                            title: "Project :",
                            name: "Onur Karademir",
                            text: "It has been made for educational purposes. 2022@"
                        )
                    }
                    .padding()
                    VStack (alignment: .center, spacing: 20) {
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width - 200 : UIScreen.main.bounds.width - 350, height: 10)
                            .background(.red)
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width - 150 : UIScreen.main.bounds.width - 350, height: 10)
                            .background(.red)
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width - 100 : UIScreen.main.bounds.width - 350, height: 10)
                            .background(.red)
                        HStack{Spacer()}
                            .frame(width: isLine ? UIScreen.main.bounds.width : UIScreen.main.bounds.width - 350,  height: 10)
                            .background(.red)
                    }
                }
                .offset(x: 0, y: isLine ? 0 : -45)
                .opacity(isLine ? 1 : 0)
                .onAppear {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        withAnimation(.easeInOut(duration: 0.7)) {
                            isLine.toggle()
                        }
                    }
                }
                
            }
        }
        .background(.black)
    }
}
struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}
