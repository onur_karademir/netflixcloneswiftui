//
//  LaunchScreenView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct LaunchScreenView: View {
    let pageColor : Color = Color.black
    var body: some View {
        ZStack (alignment: .center) {
            //pageColor//
            pageColor
            VStack (spacing: -25) {
                //Customlogo//
                NetflixTitleView(fontSize: 180, netflixN: "N")
                //netflix title//
                NetflixLogoView(color: .red, title: "Netflix", fontSize: 60, kerning: 4, fontWeight: .bold)
            }
            .offset(x: 0, y: -35)
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .ignoresSafeArea()
    }
}

struct LaunchScreenView_Previews: PreviewProvider {
    static var previews: some View {
        LaunchScreenView()
    }
}
