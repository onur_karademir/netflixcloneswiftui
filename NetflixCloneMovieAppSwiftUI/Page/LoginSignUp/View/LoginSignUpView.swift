//
//  LoginSignUpView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct LoginSignUpView: View {
    @Binding var show : Bool
    @State var email = ""
    @State var pass = ""
    @State var visible = false
    @State var alert = false
    @State var error = ""
    var body: some View {
        ZStack {
            Image("LaunchImage")
                .resizable()
                .scaledToFill()
                .frame(width: UIScreen.main.bounds.width)
                .clipped()
                .ignoresSafeArea()
            
            VStack {
                HStack {
                    Spacer()
                    Button {
                        self.show.toggle()
                    } label: {
                        RegisterButtonView(regImage: "person.fill", regText: "Register")
                    }
                }
                .padding(.horizontal)
                .padding(.bottom)
                NetflixTitleView(fontSize: 65, netflixN: "NETFLIX")
                    .padding()
                    .border(.red, width: 5)
                LoginSubTitleView(subTitle: "Log in to your account", color: .white)
                
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        VStack {
                            ZStack (alignment: .leading) {
                                if email.isEmpty {
                                    Text("Email")
                                        .foregroundColor(.red)
                                        .padding()
                                        .padding(.top, 25)
                                }
                                TextField("Email", text: self.$email)
                                    .foregroundColor(Color.white)
                                    .autocapitalization(.none)
                                    .padding()
                                    .background(RoundedRectangle(cornerRadius: 4).stroke(self.email != "" ? .pink : .white, lineWidth: 2))
                                    .padding(.top, 25)
                            }
                        }
                        
                        VStack {
                            ZStack (alignment: .leading) {
                                if pass.isEmpty {
                                    Text("Password")
                                        .foregroundColor(.red)
                                        .padding()
                                        .padding(.top, 25)
                                }
                                HStack(spacing: 15) {
                                    
                                    VStack {
                                        
                                        if self.visible{
                                            
                                            TextField("Password", text: self.$pass)
                                            .autocapitalization(.none)
                                            .foregroundColor(Color.white)
                                        }
                                        else{
                                            
                                            SecureField("Password", text: self.$pass)
                                            .autocapitalization(.none)
                                            .foregroundColor(Color.white)
                                        }
                                    }
                                    // button password visible or isVisible //
                                    Button {
                                        self.visible.toggle()
                                    } label: {
                                        Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                                            .foregroundColor(.white)
                                    }
                                }
                                .padding()
                                .background(RoundedRectangle(cornerRadius: 4).stroke(self.pass != "" ? .pink : .white,lineWidth: 2))
                                .padding(.top, 25)
                            }
                            HStack {
                                
                                Spacer()
                                // reset password button //
                                Button {
                                    //self.reset()
                                } label: {
                                    RegisterButtonView(regImage: "person.fill.questionmark", regText: "Forget password")
                                }
                                .padding(.top, 20)
                            }
                            Button {
                                //self.verify()
                            } label: {
                                CustomLoginButtonView(buttonText: "Log in", buttonTextColor: .white, buttonWidth: UIScreen.main.bounds.width - 50, buttonRadius: 10, buttonBg: .red)
                            }
                            
                            .padding(.top, 25)
                        }
                    }
                    .padding(.horizontal)
                    Spacer()

                }
                
            }
            .background(.black.opacity(0.65))
            if self.alert{
                ErrorView(alert: self.$alert, error: self.$error)
            }
        }
    }
//    func verify() {
//
//        if self.email != "" && self.pass != "" {
//
//            Auth.auth().signIn(withEmail: self.email, password: self.pass) { (res, err) in
//
//                if err != nil {
//
//                    self.error = err!.localizedDescription
//                    self.alert.toggle()
//                    return
//                }
//
//                if let user = Auth.auth().currentUser {
//                    if user.isEmailVerified == true {
//                        print("success")
//                        UserDefaults.standard.set(true, forKey: "status")
//                        NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
//                    } else {
//                        self.error = "E-mail address is not verified."
//                        self.alert.toggle()
//                    }
//                }
//
//
//            }
//        }
//        else {
//
//            self.error = "Please fill all the contents properly"
//            self.alert.toggle()
//        }
//    }
//
//    func reset() {
//
//        if self.email != "" {
//
//            Auth.auth().sendPasswordReset(withEmail: self.email) { (err) in
//
//                if err != nil {
//
//                    self.error = err!.localizedDescription
//                    self.alert.toggle()
//                    return
//                }
//
//                self.error = "RESET"
//                self.alert.toggle()
//            }
//        }
//        else{
//
//            self.error = "Email Id is empty"
//            self.alert.toggle()
//        }
//    }
}

struct LoginSignUpView_Previews: PreviewProvider {
    static var previews: some View {
        LoginSignUpView(show: .constant(false))
        //SignUp(show: .constant(false))
    }
}


struct SignUp : View {
    
    @State var color = Color.black.opacity(0.7)
    @State var name = ""
    @State var email = ""
    @State var pass = ""
    @State var repass = ""
    @State var visible = false
    @State var revisible = false
    @Binding var show : Bool
    @State var alert = false
    @State var error = ""
    
    var body: some View{
        
        ZStack {
            Image("LaunchImage")
                .resizable()
                .scaledToFill()
                .frame(width: UIScreen.main.bounds.width)
                .clipped()
                .ignoresSafeArea()
            
            VStack {
                
                HStack {
                    Button {
                        self.show.toggle()
                    } label: {
                        RegisterButtonView(regImage: "chevron.left", regText: "Back")
                    }
                    .padding(10)
                    Spacer()
                }
                .padding(.horizontal)
                
                NetflixTitleView(fontSize: 65, netflixN: "NETFLIX")
                    .padding()
                    .border(.red, width: 5)
                LoginSubTitleView(subTitle: "Register in to your account", color: .white)
                //scroll//
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        //email//
                        VStack {
                            ZStack (alignment: .leading) {
                                if email.isEmpty {
                                    Text("Email")
                                        .foregroundColor(.red)
                                        .padding()
                                        .padding(.top, 25)
                                }
                                TextField("Email", text: self.$email)
                                    .foregroundColor(Color.white)
                                    .autocapitalization(.none)
                                    .padding()
                                    .background(RoundedRectangle(cornerRadius: 4).stroke(self.email != "" ? .pink : .white, lineWidth: 2))
                                    .padding(.top, 25)
                            }
                        }
                        //email//
                        VStack {
                            ZStack (alignment: .leading) {
                                if name.isEmpty {
                                    Text("Name")
                                        .foregroundColor(.red)
                                        .padding()
                                        .padding(.top, 25)
                                }
                                TextField("Name", text: self.$name)
                                    .foregroundColor(Color.white)
                                    .autocapitalization(.none)
                                    .padding()
                                    .background(RoundedRectangle(cornerRadius: 4).stroke(self.name != "" ? .pink : .white, lineWidth: 2))
                                    .padding(.top, 25)
                            }
                        }
                        //password//
                        VStack {
                            ZStack (alignment: .leading) {
                                if pass.isEmpty {
                                    Text("Password")
                                        .foregroundColor(.red)
                                        .padding()
                                        .padding(.top, 25)
                                }
                                HStack(spacing: 15) {
                                    
                                    VStack {
                                        
                                        if self.visible{
                                            
                                            TextField("Password", text: self.$pass)
                                            .autocapitalization(.none)
                                            .foregroundColor(Color.white)
                                        }
                                        else{
                                            
                                            SecureField("Password", text: self.$pass)
                                            .autocapitalization(.none)
                                            .foregroundColor(Color.white)
                                        }
                                    }
                                    // button password visible or isVisible //
                                    Button {
                                        self.visible.toggle()
                                    } label: {
                                        Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                                            .foregroundColor(.white)
                                    }
                                }
                                .padding()
                                .background(RoundedRectangle(cornerRadius: 4).stroke(self.pass != "" ? .pink : .white,lineWidth: 2))
                                .padding(.top, 25)
                            }
                            
                        }
                        //re-password//
                        VStack {
                            ZStack (alignment: .leading) {
                                if repass.isEmpty {
                                    Text("Re-enter")
                                        .foregroundColor(.red)
                                        .padding()
                                        .padding(.top, 25)
                                }
                                HStack(spacing: 15) {
                                    
                                    VStack {
                                        
                                        if self.revisible{
                                            
                                            TextField("Re-enter", text: self.$repass)
                                            .autocapitalization(.none)
                                            .foregroundColor(Color.white)
                                        }
                                        else{
                                            
                                            SecureField("Re-enter", text: self.$repass)
                                            .autocapitalization(.none)
                                            .foregroundColor(Color.white)
                                        }
                                    }
                                    // button password visible or isVisible //
                                    Button {
                                        self.revisible.toggle()
                                    } label: {
                                        Image(systemName: self.revisible ? "eye.slash.fill" : "eye.fill")
                                            .foregroundColor(.white)
                                    }
                                }
                                .padding()
                                .background(RoundedRectangle(cornerRadius: 4).stroke(self.repass != "" ? .pink : .white,lineWidth: 2))
                                .padding(.top, 25)
                            }
                            
                        }
                        Button {
                            //self.register()
                        } label: {
                            CustomLoginButtonView(buttonText: "Register", buttonTextColor: .white, buttonWidth: UIScreen.main.bounds.width - 50, buttonRadius: 10, buttonBg: .red)
                        }
                        .padding(.top, 25)
                    }
                    .padding(.horizontal)
                    Spacer()
                }
            }
            .background(.black.opacity(0.65))
            if self.alert{
                ErrorView(alert: self.$alert, error: self.$error)
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
//    func createUserInfo() {
//        let db = Firestore.firestore()
//        let Email = Auth.auth().currentUser?.email!
//        let UserId = Auth.auth().currentUser?.uid
//        db.collection("Users").document("\(UserId!)").setData(["name" : name, "email" : Email!])
//    }
//    func register() {
//
//        if self.email != "" {
//
//            if self.pass == self.repass {
//
//                Auth.auth().createUser(withEmail: self.email, password: self.pass) { (res, err) in
//                    Auth.auth().currentUser?.sendEmailVerification { (error) in
//                            if err != nil {
//
//                                self.error = err!.localizedDescription
//                                self.alert.toggle()
//                                return
//                            }
//                        }
//                    if err != nil{
//
//                        self.error = err!.localizedDescription
//                        self.alert.toggle()
//                        return
//                    }
//                    //self.error = "We sent a verification e-mail. Check your e-mail address."
//                    self.error = "VER"
//                    self.alert.toggle()
////                    print("success")
////
////                    UserDefaults.standard.set(true, forKey: "status")
////                    NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
//                    //self.createUserInfo()
//                }
//            }
//            else {
//
//                self.error = "Password mismatch"
//                self.alert.toggle()
//            }
//        }
//        else{
//
//            self.error = "Please fill all the contents properly"
//            self.alert.toggle()
//        }
//    }
}



struct ErrorView : View {
    
    @State var color = Color.black.opacity(0.7)
    @Binding var alert : Bool
    @Binding var error : String
    
    var body: some View{
        
        VStack {
            
            VStack {
                
                HStack{
                    
                    Text(self.error == "RESET" ? "Message" : self.error == "VER" ? "Message" : "Error")
                    //Text("Message")
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(self.color)
                    
                    Spacer()
                }
                .padding(.horizontal, 25)
                
                Text(self.error == "RESET" ? "Password reset link has been sent successfully" : self.error == "VER" ? "Verification link has been sent to your e-mail address. Please check your e-mail address." : self.error)
                .foregroundColor(self.color)
                .padding(.top)
                .padding(.horizontal, 25)
                
                Button(action: {
                    
                    self.alert.toggle()
                    
                }) {
                    
                    Text(self.error == "RESET" ? "Ok" : self.error == "VER" ? "Ok" : "Cancel")
                        .foregroundColor(.white)
                        .padding(.vertical)
                        .frame(width: UIScreen.main.bounds.width - 120)
                }
                .background(.red)
                .cornerRadius(10)
                .padding(.top, 25)
                
            }
            .padding(.vertical, 25)
            .frame(width: UIScreen.main.bounds.width - 70, alignment: .center)
            .background(Color.white)
            .cornerRadius(15)
        }
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .center
            )
        .background(Color.black.opacity(0.35).edgesIgnoringSafeArea(.all))
    }
}

