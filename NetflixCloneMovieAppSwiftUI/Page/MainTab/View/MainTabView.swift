//
//  MainTabView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct MainTabView: View {
    @State var selectedtab : Int = 0
    var body: some View {
        TabView (selection: $selectedtab) {
            HomeView()
                .tabItem {
                Image(systemName: "house")
                    Text("HOME")
                }.tag(0)
            // setting environment Object //
            
              // ?????????
            
            // ----- //
            ProfileView()
                .tabItem {
                Image(systemName: "person.fill")
                    Text("sssss")
                }.tag(1)
        }
        .onAppear {
            let appearance = UITabBarAppearance()
            appearance.backgroundEffect = UIBlurEffect(style: .systemUltraThinMaterial)
            appearance.backgroundColor = UIColor(Color.black.opacity(0.8))
            
            // Use this appearance when scrolling behind the TabView:
            UITabBar.appearance().standardAppearance = appearance
            // Use this appearance when scrolled all the way up:
            UITabBar.appearance().scrollEdgeAppearance = appearance
        }
        .accentColor(.red)
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
