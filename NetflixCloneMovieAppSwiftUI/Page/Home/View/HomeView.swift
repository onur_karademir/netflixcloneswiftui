//
//  HomeView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct HomeView: View {
    @StateObject var viewModel = MovieViewModel()
    @StateObject var topRatedViewModel = TopRatedViewModel()
    @State var isOpenSheet : Bool = false
    var body: some View {
        VStack (alignment:.leading, spacing: 0) {
            //header//
            HomePageHeaderView(title: "Home", image: "info", titleSize: 20, isOpenSheet: $isOpenSheet)
            ScrollView {
                VStack (alignment:.leading) {
                    TitleView(title: "Popular Content")
                    UnderLineView(color: .gray, height: 1)
                    ScrollView (.horizontal, showsIndicators: false) {
                        LazyHStack (spacing: 16) {
                            ForEach(viewModel.movies, id:\.id) { item in
                                
                                CardView(
                                    titleM: item.title,
                                    image: item.posterPath,
                                    fontSize: 20,
                                    playImage: "play.circle",
                                    heartImage: "plus.bubble",
                                    infoImage: "list.bullet.circle",
                                    desc: item.overview,
                                    backDropPath: item.backdropPath,
                                    voteCount: item.voteCount,
                                    voteAverage: item.voteAverage,
                                    releaseDate: item.releaseDate,
                                    originalLanguage: item.originalLanguage
                                )
                                .environmentObject(viewModel)
                                
                            }
                        }
                    }
                    TitleView(title: "Top Rated")
                    UnderLineView(color: .gray, height: 1)
                    ScrollView (.vertical, showsIndicators: false) {
                        LazyVStack (spacing: 16) {
                            ForEach(topRatedViewModel.movies, id:\.id) { item in
                                TopRatedView(
                                    image: item.posterPath,
                                    title: item.title,
                                    over: item.overview,
                                    playImageTopRated: "play.circle",
                                    backDropPath: item.backdropPath,
                                    voteCount: item.voteCount,
                                    voteAverage: item.voteAverage,
                                    releaseDate: item.releaseDate,
                                    originalLanguage: item.originalLanguage
                                )
                                .environmentObject(viewModel)
                            }
                        }
                    }
                }
                
            }
        }
        .navigationTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
        .padding()
        .background(.black).opacity(0.91)
        .sheet(isPresented: $isOpenSheet) {
            InfoView()
        }
    }
    
    func extractImage(data: String) -> URL {
        let path = "https://image.tmdb.org/t/p/original"
        let ext = data
        
        return URL(string: "\(path)\(ext)")!
    }
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
