//
//  CustomLoginButtonView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct CustomLoginButtonView: View {
    
    let buttonText : String
    let buttonTextColor : Color
    let buttonWidth : CGFloat
    let buttonRadius : CGFloat
    let buttonBg : Color
    var body: some View {
        Text(buttonText)
            .foregroundColor(buttonTextColor)
            .padding(.vertical)
            .frame(width: buttonWidth)
            .background(buttonBg)
            .cornerRadius(buttonRadius)
    }
    
}

struct CustomLoginButtonView_Previews: PreviewProvider {
    static var previews: some View {
        CustomLoginButtonView(buttonText: "Log out", buttonTextColor: .white, buttonWidth: UIScreen.main.bounds.width - 50, buttonRadius: 10, buttonBg: .pink)
    }
}
