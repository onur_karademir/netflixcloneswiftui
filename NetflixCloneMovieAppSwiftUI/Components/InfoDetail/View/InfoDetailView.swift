//
//  InfoDetailView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct InfoDetailView: View {
    let image : String
    let title : String
    let name : String
    let text : String
    var body: some View {
        HStack {
            Circle()
                .fill(Color.red)
                .frame(width: 100, height: 100, alignment: .center)
                .overlay(
                    Image(systemName: image)
                        .font(.largeTitle)
                )
            Spacer()
            VStack (alignment: .leading) {
                Text(title)
                    .font(.title3)
                Text(name)
                    .font(.title)
                Text(text)
            }
            Spacer()
        }
        .padding(6)
        .background(.ultraThinMaterial)
        .cornerRadius(17)

    }
}

struct InfoDetailView_Previews: PreviewProvider {
    static var previews: some View {
        InfoDetailView(image: "person.fill", title: "Project :", name: "Onur Karademir", text: "It has been made for educational purposes. 2022@")
    }
}

