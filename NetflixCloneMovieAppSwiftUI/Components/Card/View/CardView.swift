//
//  CardView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct CardView: View {
    let titleM : String
    let image : String
    let fontSize : CGFloat
    let playImage : String
    let heartImage : String
    let infoImage : String
    let desc : String
    let backDropPath : String
    let voteCount : Int
    let voteAverage : Double
    let releaseDate : String
    let originalLanguage : String
    @State var isLike : Bool = false
    @EnvironmentObject var homeData : MovieViewModel
    var body: some View {
        VStack (alignment: .leading) {
            //WebImage(url: extractImage(data: image))
            Image("image")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 300)
                .clipped()
                .cornerRadius(20)
            VStack (alignment: .leading, spacing: 8) {
                Text(titleM)
                    .font(.system(size: fontSize, weight: .semibold, design: .rounded))
                    .foregroundColor(.white)
                    .lineLimit(1)
                HStack (spacing: 18) {
                    Image(systemName: heartImage)
                        .foregroundColor(.white)
                        .onTapGesture {
                            withAnimation {
                                isLike.toggle()
                            }
                            if isLike {
                                homeData.addLikes(like:
                                    Likes(
                                        title: titleM,
                                        overview: desc,
                                        posterPath: image,
                                        backdropPath: backDropPath,
                                        voteCount: voteCount,
                                        voteAverage: voteAverage,
                                        releaseDate: releaseDate,
                                        originalLanguage: originalLanguage
                                    )
                                )
                                print("save likes array -->> \(homeData.saveLikes)")
                            }
                            else {
                                //delete perform like array//
                                if !homeData.saveLikes.isEmpty {
                                    if let index = homeData.saveLikes.firstIndex(where: {$0.title == titleM}) {
                                        homeData.saveLikes.remove(at: index)
                                    }
                                }
                            }
                        }
                    NavigationLink {
                        
                    } label: {
                        Image(systemName: infoImage)
                            .foregroundColor(.white)
                    }

                }
                .font(.title3)
            }
            .padding(.horizontal)
        }
        .frame(maxWidth: 200)
        
        //.background(.black)
        .overlay(
            NavigationLink {
               
            } label: {
                Image(systemName: playImage)
                    .foregroundColor(.white)
                    .font(.largeTitle)
                    .background(.red)
                    .clipShape(Circle())
                    .scaleEffect(1.2)
                    .offset(x: -10, y: 10)
                    
            } , alignment: .topTrailing
        )
    }
    func extractImage(data: String) -> URL {
        let path = "https://image.tmdb.org/t/p/original"
        let ext = data
        
        return URL(string: "\(path)\(ext)")!
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(titleM: "title", image: "image", fontSize: 25, playImage: "play.circle", heartImage: "heart.fill", infoImage: "info.circle", desc: "desc", backDropPath: "backDropPath", voteCount: 1, voteAverage: 1.1, releaseDate: "20.20.20", originalLanguage: "En")
    }
}
