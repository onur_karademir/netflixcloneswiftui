//
//  HomePageHeaderView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct HomePageHeaderView: View {
    let title : String
    let image : String
    let titleSize : CGFloat
    @Binding var isOpenSheet : Bool
    var body: some View {
        HStack {
            NetflixTitleView(fontSize: 38, netflixN: "N")
            Spacer()
            Text(title)
                .foregroundColor(.white)
                .font(.system(size: titleSize, weight: .semibold, design: .rounded))
            Spacer()
            Button {
                isOpenSheet.toggle()
            } label: {
                Image(systemName: image)
                    .foregroundColor(.red)
                    .font(.title)
            }
        }
        .padding(0)
    }
}

struct HomePageHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HomePageHeaderView(title: "Home", image: "info", titleSize: 20, isOpenSheet: .constant(false))
    }
}
