//
//  RegisterButtonView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct RegisterButtonView: View {
    let regImage : String
    let regText : String
    var body: some View {
        HStack {
            Image(systemName: regImage)
            Text(regText)
                .fontWeight(.bold)
        }
        .foregroundColor(.white)
        .padding(.vertical, 9)
        .padding(.horizontal, 9)
        .background(.red)
        .clipShape(Capsule())
    }
}

struct RegisterButtonView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterButtonView(regImage: "person.fill", regText: "Register")
    }
}
