//
//  LoginSubTitleView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct LoginSubTitleView: View {
    let subTitle : String
    let color : Color
    var body: some View {
        Text(subTitle)
            .font(.title)
            .fontWeight(.bold)
            .foregroundColor(color)
            .padding(.top, 5)
    }
}

struct LoginSubTitleView_Previews: PreviewProvider {
    static var previews: some View {
        LoginSubTitleView(subTitle: "Log in to your account", color: .black)
    }
}
