//
//  NavHeaderSetView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct NavHeaderSetView: View {
    
    @Environment(\.presentationMode) var presentationMode
    let btnImage : String
    let headerLogo : String
    let isBackButtonVisible : Bool
    @State var isLogo : Bool = false
    
    var body: some View {
        HStack {
            if isBackButtonVisible {
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: btnImage)
                        .foregroundColor(.white)
                        .padding(.horizontal, 10)
                        .padding(.vertical, 5)
                        .background(.red)
                        .clipShape(Capsule())
                }
                .offset(x: isLogo ? 0 : -58, y: 0)
                .opacity(isLogo ? 1 : 0.4)
            }
            Spacer()
            Circle()
                .stroke(.red, lineWidth: 5)
                .frame(width: 40, height: 40, alignment: .center)
                .overlay(
                    Text(headerLogo)
                        .foregroundColor(.red)
                        .font(.system(size: 29, weight: .bold, design: .rounded))
                )
                .offset(x: isLogo ? 0 : 58, y: 0)
                .opacity(isLogo ? 1 : 0.4)
                .onAppear {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        withAnimation(.easeInOut(duration: 0.7)) {
                            isLogo = true
                        }
                    }
                }
                .onDisappear {
                    isLogo = false
                }
        }
        .padding(.horizontal)
        
    }
}

struct NavHeaderSetView_Previews: PreviewProvider {
    static var previews: some View {
        NavHeaderSetView(btnImage: "arrow.left", headerLogo: "N", isBackButtonVisible: true)
    }
}

