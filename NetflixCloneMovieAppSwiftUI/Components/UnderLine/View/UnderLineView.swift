//
//  UnderLineView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct UnderLineView: View {
    let color : Color
    let height : CGFloat
    var body: some View {
        HStack{Spacer()}
            .frame(height: height)
            .padding(.horizontal)
            .background(color)
            .cornerRadius(20)
    }
}

struct UnderLineView_Previews: PreviewProvider {
    static var previews: some View {
        UnderLineView(color: .gray, height: 1)
    }
}

