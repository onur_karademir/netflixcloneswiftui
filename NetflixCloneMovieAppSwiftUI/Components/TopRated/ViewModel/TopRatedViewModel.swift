//
//  TopRatedViewModel.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import Foundation

struct TopRatedResponse: Codable {
    let results : [TopRatedResult]
}

struct TopRatedResult: Codable {
    let id : Int
    let title : String
    let overview : String
    let posterPath : String
    let backdropPath : String
    let voteCount : Int
    let voteAverage : Double
    let releaseDate : String
    let originalLanguage : String

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case voteCount = "vote_count"
        case voteAverage = "vote_average"
        case releaseDate = "release_date"
        case originalLanguage = "original_language"
    }
}

class TopRatedViewModel : ObservableObject {
    
    @Published var movies : [TopRatedResult] = []
    
    init() {
        getData()
    }
    
    func getData() {
        let url = URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=8da90013ced086fb710990c305527859&language=en-US&page=1")
        
        let session = URLSession(configuration: .default)
        session.dataTask(with: url!) { data, _, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let APIData = data else {
                print("no data found")
                return
            }
            
            do {
                // decoding //
                
                let myData = try JSONDecoder().decode(TopRatedResponse.self, from: APIData)
                
                DispatchQueue.main.async {
                    self.movies = myData.results
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
}
