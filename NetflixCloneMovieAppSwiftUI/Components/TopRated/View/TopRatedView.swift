//
//  TopRatedView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct TopRatedView: View {
    let image : String
    let title : String
    let over : String
    let playImageTopRated : String
    let backDropPath : String
    let voteCount : Int
    let voteAverage : Double
    let releaseDate : String
    let originalLanguage : String
    @State var isLike : Bool = false
    @EnvironmentObject var homeData : MovieViewModel
    var body: some View {
        VStack (alignment: .leading) {
            HStack (alignment: .top) {
                Image(image)
                //WebImage(url: extractImage(data: image))
                    .resizable()
                    .scaledToFit()
                    .frame(width: 120 ,alignment: .center)
                    .clipped()
                    .clipShape(RoundedRectangle(cornerRadius: 20))
                VStack (alignment: .leading, spacing: 10) {
                    Text(title)
                        .font(.system(size: 23, weight: .semibold, design: .rounded))
                        .lineLimit(1)
                        .foregroundColor(.white)
                    Text(over)
                        .font(.system(size: 18, weight: .light, design: .rounded))
                        .lineLimit(5)
                        .foregroundColor(.gray)
                }
                
                VStack (spacing: 10) {
                    
                    ImdbView(imdbText: "imdb", imdbNote: "\(voteAverage)", imdbIcon: "bookmark.fill")
                    
                    NavigationLink {
                        
                    } label: {
                        Image(systemName: playImageTopRated)
                            .foregroundColor(.white)
                            .font(.largeTitle)
                            .background(.red)
                            .clipShape(Circle())
                            .scaleEffect(1.1)
                    }
                    Image(systemName: "plus.bubble")
                        .foregroundColor(.white)
                        .onTapGesture {
                            withAnimation {
                                isLike.toggle()
                            }
                            if isLike {
                                homeData.addLikes(like:
                                    Likes(
                                        title: title,
                                        overview: over,
                                        posterPath: image,
                                        backdropPath: backDropPath,
                                        voteCount: voteCount,
                                        voteAverage: voteAverage,
                                        releaseDate: releaseDate,
                                        originalLanguage: originalLanguage
                                    )
                                )
                                print("save likes array -->> \(homeData.saveLikes)")
                            }
                            else {
                                //delete perform like array//
                                if !homeData.saveLikes.isEmpty {
                                    if let index = homeData.saveLikes.firstIndex(where: {$0.title == title}) {
                                        homeData.saveLikes.remove(at: index)
                                    }
                                }
                            }
                        }
                    NavigationLink {
                       
                    } label: {
                        Image(systemName: "list.bullet.circle")
                            .foregroundColor(.white)
                    }
                }
            }
            .padding(.vertical)
        }
        .padding(.horizontal)
        //.background(.black)
        .alert(isPresented: $isLike) {
            Alert(
                title: Text("Added to watch list.")
            )
        }
        
    }
    func extractImage(data: String) -> URL {
        let path = "https://image.tmdb.org/t/p/original"
        let ext = data
        
        return URL(string: "\(path)\(ext)")!
    }
}
extension Image {
    func centerCropped() -> some View {
        GeometryReader { geo in
            self
            .resizable()
            .scaledToFill()
            .frame(width: geo.size.width, height: geo.size.height)
            .clipped()
        }
    }
}
struct TopRatedView_Previews: PreviewProvider {
    static var previews: some View {
        TopRatedView(image: "image", title: "titletitletitletitle", over: "over", playImageTopRated: "play.circle", backDropPath: "backDropPath", voteCount: 1, voteAverage: 1.1, releaseDate: "20.20.20", originalLanguage: "En")
    }
}
