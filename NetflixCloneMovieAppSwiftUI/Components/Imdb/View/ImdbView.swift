//
//  ImdbView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct ImdbView: View {
    let imdbText : String
    let imdbNote : String
    let imdbIcon : String
    var body: some View {
        Circle()
            .frame(width: 50, height: 50)
            .foregroundColor(.yellow)
            .overlay(
                VStack (spacing: 0) {
                    Text(imdbText)
                        .font(.caption)
                    Text(imdbNote)
                        .font(.caption)
                        .fontWeight(.bold)
                    Image(systemName: imdbIcon)
                        .font(.caption2)
                }
                    .foregroundColor(.black)
            )
    }
}

struct ImdbView_Previews: PreviewProvider {
    static var previews: some View {
        ImdbView(imdbText: "imdb", imdbNote: "8.9", imdbIcon: "bookmark.fill")
    }
}

