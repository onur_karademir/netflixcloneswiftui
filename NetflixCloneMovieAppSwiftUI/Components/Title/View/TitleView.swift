//
//  TitleView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct TitleView: View {
    let title : String
    var body: some View {
        Text(title)
            .foregroundColor(.white)
             .font(.title)
    }
}

struct TitleView_Previews: PreviewProvider {
    static var previews: some View {
        TitleView(title: "Title")
    }
}
