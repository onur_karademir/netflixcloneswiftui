//
//  NetflixLogoView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct NetflixLogoView: View {
    let color : Color
    let title : String
    let fontSize : CGFloat
    let kerning : CGFloat
    let fontWeight : Font.Weight
    var body: some View {
        Text(title)
            .foregroundColor(color)
            .font(.system(size: fontSize, weight: .bold, design: .rounded))
            .fontWeight(fontWeight)
            .kerning(kerning)
    }
}

struct NetflixLogoView_Previews: PreviewProvider {
    static var previews: some View {
        NetflixLogoView(color: .red, title: "Netflix", fontSize: 60, kerning: 4, fontWeight: .bold)
    }
}
