//
//  SettingsView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct SettingsView: View {
    let sectionName : String
    @Binding var selected: Int
    @Binding var dataArray : [String]
    var body: some View {
        Picker(selection: self.$selected, label: Text(sectionName)) {
            ForEach(0 ..< self.dataArray.count, id:\.self) {
                    Text(self.dataArray[$0]).tag($0)
                }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(sectionName: "Currency", selected: .constant(0), dataArray: .constant([]))
    }
}
