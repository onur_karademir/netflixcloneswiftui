//
//  UserIconView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct UserIconView: View {
    var body: some View {
        VStack {
            Rectangle()
                .fill(.orange)
                .frame(width: 150, height: 150, alignment: .center)
                .cornerRadius(9)
                .overlay(
                    Circle()
                        .frame(width: 20, height: 20, alignment: .center)
                        .offset(x: -40, y: -20)
                    ,
                    alignment: .center
                )
                .overlay(
                    Circle()
                        .fill(.orange)
                        .frame(width: 7, height: 7, alignment: .center)
                        .offset(x: -40, y: -20)
                    ,
                    alignment: .center
                )
                
                .overlay(
                    Circle()
                        .frame(width: 20, height: 20, alignment: .center)
                        .offset(x: 40, y: -20)
                    ,
                    alignment: .center
                )
            
                .overlay(
                    Circle()
                        .fill(.orange)
                        .frame(width: 7, height: 7, alignment: .center)
                        .offset(x: 40, y: -20)
                    ,
                    alignment: .center
                )
            
                .overlay(
                    Rectangle()
                        .frame(width: 10, height: 25, alignment: .center)
                        .offset(x: 0, y: 0)
                    ,
                    alignment: .center
                )
            
                .overlay(
                    Capsule()
                        .fill(.red)
                        .frame(width: 100, height: 30, alignment: .center)
                        .offset(x: 0, y: -20)
                    ,
                    alignment: .bottom
                )
                .overlay(
                    Rectangle()
                        .fill(.orange)
                        .frame(width: 10, height: 15, alignment: .center)
                        .offset(x: -30, y: -35)
                    ,
                    alignment: .bottom
                )
                .overlay(
                    Rectangle()
                        .fill(.orange)
                        .frame(width: 10, height: 15, alignment: .center)
                        .offset(x: 0, y: -35)
                    ,
                    alignment: .bottom
                )
                .overlay(
                    Rectangle()
                        .fill(.orange)
                        .frame(width: 10, height: 15, alignment: .center)
                        .offset(x: 30, y: -35)
                    ,
                    alignment: .bottom
                )
                .overlay(
                    Rectangle()
                        .fill(.orange)
                        .frame(width: 10, height: 15, alignment: .center)
                        .offset(x: 15, y: -20)
                    ,
                    alignment: .bottom
                )
                .overlay(
                    Rectangle()
                        .fill(.orange)
                        .frame(width: 10, height: 15, alignment: .center)
                        .offset(x: -15, y: -20)
                    ,
                    alignment: .bottom
                )
        }
    }
}

struct UserIconView_Previews: PreviewProvider {
    static var previews: some View {
        UserIconView()
    }
}
