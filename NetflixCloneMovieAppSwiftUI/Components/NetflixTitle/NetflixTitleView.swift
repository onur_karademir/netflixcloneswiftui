//
//  NetflixTitleView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct NetflixTitleView: View {
    let fontSize : CGFloat
    let netflixN : String
    var body: some View {
        Text(netflixN)
            .font(.system(size: fontSize, weight: .bold, design: .rounded))
            .italic()
            .foregroundColor(.red)
            .kerning(0)
    }
}

struct NetflixTitleView_Previews: PreviewProvider {
    static var previews: some View {
        NetflixTitleView(fontSize: 180, netflixN: "N")
    }
}
