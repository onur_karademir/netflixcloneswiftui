//
//  ContentView.swift
//  NetflixCloneMovieAppSwiftUI
//
//  Created by Onur on 8.11.2022.
//

import SwiftUI

struct ContentView: View {
    @State var isLaunchScreen : Bool = true
    @State var show = false
    @State var isUserLogin : Bool = true
    var body: some View {
        ZStack (alignment: .top) {
            if isLaunchScreen {
                LaunchScreenView()
                    .opacity(isLaunchScreen ? 1 : 0)
                    .onAppear {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
                            withAnimation(.easeInOut(duration: 0.8)) {
                            self.isLaunchScreen = false
                          }
                        }
                    }
            } else {
                if isUserLogin {
                    MainTabView()
                } else {
                    ZStack{
                        
                        NavigationLink(destination: SignUp(show: self.$show), isActive: self.$show) {
                            
                            Text("")
                        }
                        .hidden()
                        
                        LoginSignUpView(show: self.$show)
                    }
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
